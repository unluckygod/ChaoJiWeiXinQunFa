/*
 * ==================================================
 * 项目开发者：JerryStark
 * 开发者Email：4771007@qq.com
 * 开发者QQ:4771007
 * 开发范围：web，wap，android,ios,osx or win or linux 应用
 * ==================================================
 */
package weixin;

import java.awt.Image;
import javax.swing.ImageIcon;

/**
 * 获取保内图标资源
 *
 * @author jerry
 */
public class GetIconImage {

    /**
     * 获取icon 的资源图片
     *
     * @return
     */
    public Image GetImage() {
        ImageIcon icon = new ImageIcon(this.getClass().getResource("/images/weixin_logo.png"));

        //返回Image
        return icon.getImage();
    }
}
